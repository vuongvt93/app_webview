import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
// import MapViewScreen from './screens/MapViewScreen';
import Login from './screens/Login';
import GoWeb from './screens/GoWeb';
// import JobType from './screens/JobType';
// import JobList from './screens/JobList';
// import ESlist from './screens/ESlist';
// import Check_qrcode from './screens/Qrcode';
// import QR_Delivery from './screens/QRorDV';
// import Settinguser from './screens/settingpass';
// import Changerpass from './screens/changerpass';
// import ChangeSDT from './screens/changesdt';
// import Items from './screens/Items';
// import Waitupdate from './screens/waitupdate';
// import Eventupdate from './screens/EventUpdate';

const MainNavigator = createStackNavigator({
  Home: {screen: GoWeb,
    navigationOptions: {
      // title: 'Home',
      // header: null //this will hide the header
      headerShown: false
    },
  },
  // Home: {screen: Login},
  // QR_Delivery: {screen: QR_Delivery},
  // Settinguser: {screen: Settinguser}, 
  // Changerpass: {screen: Changerpass}, 
  // ChangeSDT: {screen: ChangeSDT}, 
  // Qrcode: {screen: Check_qrcode}, 
  // MapViewScreen: {screen: MapViewScreen},
  // JobType: {screen: JobType},
  // JobList: {screen: JobList},
  // ProductItems: {screen: Items},
  // Waitupdate: {screen: Waitupdate},
  // ESlist: {screen: ESlist},
  // Eventupdate: {screen: Eventupdate},
    // headerMode: 'none',
});

export default MainNavigator;
