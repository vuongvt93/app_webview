import React, {Component} from 'react';
import {
  Alert,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  ScrollView,
  PermissionsAndroid,
  BackHandler, 
} from 'react-native';
import {connect} from 'react-redux';
import { WebView } from 'react-native-webview';
import {RNCamera} from 'react-native-camera';
import NetInfo from '@react-native-community/netinfo';
import {useNetInfo} from "@react-native-community/netinfo";
// import HC_LOGO_NEW from '../assets/HCLOGO.png';
// import Loading from '@Loading';
import {LOADING_URL} from '../redux/actionCreators';
import AsyncStorage from '@react-native-community/async-storage';



class GoWeb  extends Component {
  constructor(props) {
    super(props);
    this.WEBVIEW_REF = React.createRef();
  }
  state = {
    URL:'',
    URLredirect:'',
    checkconnect:'',
  };

//   static navigationOptions = ({navigation}) => {
//     let headerStyle = {backgroundColor: '#4472C4', height: 50};
//     let headerLeft = () => (
//       <View style={{marginLeft: 8}}>
//         <Image source={HC_LOGO_NEW} style={{width: 40, height: 40}} />
//       </View>
//     );
//     let headerTitle = () => (
//       <View style={{alignItems: 'center', flex: 1}}>
//         <Text style={{color: '#fff', fontSize: 18}}>Test WebView</Text>
//       </View>
//     );
//     return {
//       headerStyle,
//       headerTitle,
//       headerLeft,
//     };
//   };

  
  
  async componentDidMount() {
    // this._Loading_url(); 
    this.requestPermission();
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  async componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }
  handleBackButton = ()=>{
    this.WEBVIEW_REF.current.goBack();
    return true;
  }

  onNavigationStateChange(navState) {
    this.setState({
      canGoBack: navState.canGoBack
    });
  }

  _setSate = state => {
    this.setState(state);
  };

 requestPermission = async () => {
            try {
              var permission = await PermissionsAndroid.check(
                  // PermissionsAndroid.PERMISSIONS.CAMERA && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
                  PermissionsAndroid.PERMISSIONS.CAMERA 
              );
              console.log("camera && gallery permission granted:- ", permission);
              if (!permission) {
                  const granted = await PermissionsAndroid.request(
                      PermissionsAndroid.PERMISSIONS.CAMERA
                      // PermissionsAndroid.PERMISSIONS.CAMERA && PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE
                  );
                  if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                      console.log('Camera && Gallery permissions granted');
                  } else {
                      console.log('Camera && Gallery permission denied');
                  }
              }
          } catch (err) {
              console.warn(err);
          }
 };

 
  _Loading_url = async () => {
    // const {email: P_USERNAME, password: P_PASSWORD} = this.state;
    // const netInfo = NetInfo();
    // console.log(netInfo);
    NetInfo.fetch().then(state => {
      console.log("Is connected?", state.isConnected);
          if(state.isConnected){
            this.props.LOADING_URL(
              this._setSate,
            );  
          }else {
            Alert.alert('Kiểm tra kết nối Internet');
          }

    });
    // this.props.LOADING_URL(
    //   this._setSate,
    // );  
  };
 
  render() { 
    // const P_URL = this.state.URL;
    const P_URL_test = 'https://hcerp.vn/ords/f?p=888:1';
    return (   
     
       
            <WebView                 
              source={{ uri:P_URL_test}}
              // onNavigationStateChange={this.P_URL_Redirect}
              ref={this.WEBVIEW_REF}
              onNavigationStateChange={this.onNavigationStateChange.bind(this)}
              onError={(syntheticEvent) => {
                const { nativeEvent } = syntheticEvent;              
                if(nativeEvent.loading === false){
                     this.setState({
                       URL : 'https://hc.com.vn'
                     })
                }
                console.log('WebView error: ', nativeEvent.description);
              }}             
           />    
      
                  
    );
  }
}

function mapStateToProps(state) {
  return {
    checkLogin: state.LoginReducers.success,
    P_DEL_ID: state.LoginReducers.P_DEL_ID,
    error: state.LoginReducers.error,
    isLoading: state.LoginReducers.isLoading,
  };
}

export default connect(mapStateToProps,{LOADING_URL})(GoWeb);

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // alignItems: 'center',
    // paddingTop: 5,
    // paddingHorizontal:15,
    // paddingVertical:15,
    // backgroundColor: '#fff',
  },
 
 
  // area_setting:{
  //   flex:10,
  // },
  // area_content:{
  //   flex:80,
  //   // alignItems: 'center',
  // },
  // buttonText: {
  //   fontFamily: 'Baskerville',
  //   fontSize: 15,
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   color: '#fff',
  // },
  // input: {
  //   // width: 200,

  //   // fontFamily: 'Baskerville',
  //   backgroundColor: '#fff',
  //   fontSize: 17,
  //   height: 50,
  //   padding: 10,
  //   borderWidth: 1,
  //   borderColor: '#4472C4',
  //   marginVertical: 10,
  //   color:'#000',
  // },
  // input_pass:{
  //   fontSize: 17,
  //   color:'#000',
  // },

  // area_content_pass:{
  //   flexDirection:'row',
  //   backgroundColor: '#fff',
  //   fontSize: 17,
  //   height: 50,
  //   // padding: 10,
  //   borderWidth: 1,
  //   borderColor: '#4472C4',
  //   marginVertical: 10,
  //   color:'#000',
  //   alignItems: 'center',
  //   justifyContent: 'space-between',
  // },

 
  // button: {
  //   alignItems: 'center',
  //   backgroundColor: '#4472C4',
  //   // width: 150,
  //   // height: 44,
  //   padding: 10,
  //   borderWidth: 1,
  //   borderColor: '#4472C4',
  //   borderRadius: 4,
  //   marginBottom: 10,
  //   marginTop: 20,
  //   // marginLeft: 50,
  // },
  // // buttonText: {
  // //   fontFamily: 'Baskerville',
  // //   fontSize: 16,
  // //   alignItems: 'center',
  // //   justifyContent: 'center',
  // //   color: 'black',
  // // },
 
});
