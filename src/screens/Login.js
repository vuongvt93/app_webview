import React, {Component} from 'react';
import {
  Alert,
  Text,
  TouchableOpacity,
  TextInput,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {LOGIN} from '../redux/actionCreators';
import logo_oto from '../assets/otoimage.png';
import HC_LOGO_NEW from '../assets/HCLOGO.png';
// import Loading from '@Loading';
import AsyncStorage from '@react-native-community/async-storage';




class Login extends React.Component {
  state = {
    email: '',
    password: '',
    // isLoading: false,
    // isAppReady:true,
    // isUpdateStatus:'Checking Version',

  };

  static navigationOptions = ({navigation}) => {
    let headerStyle = {backgroundColor: '#4472C4', height: 50};
    let headerLeft = () => (
      <View style={{marginLeft: 8}}>
        <Image source={HC_LOGO_NEW} style={{width: 40, height: 40}} />
      </View>
    );
    let headerTitle = () => (
      <View style={{alignItems: 'center', flex: 1}}>
        <Text style={{color: '#fff', fontSize: 18}}>HC Delivery Tracking</Text>
      </View>
    );
    return {
      headerStyle,
      headerTitle,
      headerLeft,
    };
  };

 

  async componentDidMount() {
    // this._getUserNameInAsync();
    // this._checkUpdate();
  }

  //  _getUserNameInAsync = async () => {
  //   try {
  //    const USERNAME = await AsyncStorage.getItem('USERNAME');
  //    const PASSWORD = await AsyncStorage.getItem('PASSWORD');
  //       if (USERNAME !== null){
  //         this.setState({email:USERNAME});
  //       }
  //       if (PASSWORD !== null){
  //         this.setState({password:PASSWORD});
  //       }
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  onLogin() {
    const {email, password} = this.state;

    Alert.alert('Credentials', `email: ${email} + password: ${password}`);
  }

  _setSate = state => {
    this.setState(state);
  };
  _Login = async () => {
    const {email: P_USERNAME, password: P_PASSWORD} = this.state;
    this.props.LOGIN(
      P_USERNAME,
      P_PASSWORD,
      this.props.navigation,
      this._setSate,
    );
    const getUsername = await AsyncStorage.getItem('USERNAME');
    const getPassword = await AsyncStorage.getItem('PASSWORD');
    if (getUsername == null | getUsername !== P_USERNAME){
      await AsyncStorage.setItem('USERNAME',P_USERNAME);
    }
    if (getPassword == null | getPassword !== P_PASSWORD){
      await AsyncStorage.setItem('PASSWORD',P_PASSWORD);
    }

  };

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <View style={styles.area_image}>
            <Image
              source={logo_oto}
              style={{width: 100, height: 100, marginTop: 25}}
            />
          </View>

          <View style={styles.area_content}>
            
            <TextInput
              value={this.state.email}
              onChangeText={email => this.setState({email})}
              placeholder="Username"
              placeholderTextColor="#a9a9a9"
              style={styles.input}
            />
            <TextInput
              value={this.state.password}
              onChangeText={password => this.setState({password})}
              placeholder={'Password'}
              secureTextEntry={true}
              placeholderTextColor="#a9a9a9"
              style={styles.input}
            />
            <TouchableOpacity>
              <Text style={styles.misspass}>Quên mật khẩu</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.button}
              onPress={() => this._Login()}>
              <Text style={styles.buttonText}>ĐĂNG NHẬP</Text>
            </TouchableOpacity>
         {/* 
            <View style={{position:'absolute',left:0,right:0,bottom:50,justifyContent:'center',alignItems:'center'}}>
              <Text style={{color:'#2e2e2e'}}>
                Version {DeviceInfo.getVersion()}.1.4
              </Text>
            </View> */}

          </View>
          {/* <Loading isVisible={this.state.isLoading} />
          <Loading isVisible={!this.state.isAppReady} message={this.state.isUpdateStatus}/> */}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

function mapStateToProps(state) {
  return {
    checkLogin: state.LoginReducers.success,
    P_DEL_ID: state.LoginReducers.P_DEL_ID,
    error: state.LoginReducers.error,
    isLoading: state.LoginReducers.isLoading,
  };
}

export default connect(mapStateToProps, {LOGIN})(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    // paddingTop: 15,
    backgroundColor: '#fff',
  },
  area_image: {
    flex: 25,
    alignItems: 'center',
    // backgroundColor:'yellow',
  },
  area_content: {
    flex: 75,
    alignItems: 'center',
  },

  button: {
    alignItems: 'center',
    backgroundColor: '#4472C4',
    width: 150,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: '#4472C4',
    borderRadius: 4,
    marginBottom: 10,
    // marginLeft: 50,
  },
  buttonText: {
    fontFamily: 'Baskerville',
    fontSize: 15,
    alignItems: 'center',
    justifyContent: 'center',
    color: '#fff',
  },
  input: {
    width: 180,

    // fontFamily: 'Baskerville',
    backgroundColor: '#fff',
    fontSize: 17,
    height: 50,
    padding: 10,
    borderWidth: 1,
    borderColor: '#4472C4',
    marginVertical: 10,
    color:'#000',
  },
  misspass: {
    marginBottom: 10,
  },
});
