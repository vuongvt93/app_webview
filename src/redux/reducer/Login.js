// import {FETCH_JOB, FETCH_SUCCEEDED, FETCH_FAILED } from '../actionCreators';

const Login = {
  success: [],
  P_USERNAME: '',
  isLoading: false,
  error: false,
};

const LoginReducers = (state = Login, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCEEDED':
      return {
        ...state,
        success: action.resultLogin.state,
        P_USERNAME: action.P_USERNAME,
        error: false,
        isLoading: false,
      };
    case 'LOGIN_FAILED':
      return {error: true, isLoading: false};

    default:
      return state;
  }
};

export default LoginReducers;
