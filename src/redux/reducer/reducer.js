import {combineReducers} from 'redux';
import LoginReducers from './Login';
import JobReducers from './JobReducer';
import Loadingurl from './loadingurl';

const reducer = combineReducers({
  LoginReducers : LoginReducers, 
  DataJob: JobReducers, 
  url:Loadingurl,
});

export default reducer;
