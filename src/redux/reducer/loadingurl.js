// import {FETCH_JOB, FETCH_SUCCEEDED, FETCH_FAILED } from '../actionCreators';

const urlmobile = {
    URL:'',
    error: false,
  };
  
  const Loadingurl = (state = urlmobile, action) => {
    switch (action.type) {
      case 'LOADURL_SUCCESS':
        return {
          ...state,
          URL:action.URL,
        //   success: action.resultLogin.state,
        //   P_USERNAME: action.P_USERNAME,
          error: false,
         
        };
      case 'LOADURL_FAIL':
        return {error: true};
  
      default:
        return state;
    }
  };
  
  export default Loadingurl;
  