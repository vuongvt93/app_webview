// import {FETCH_JOB, FETCH_SUCCEEDED, FETCH_FAILED } from '../actionCreators';

const defaultState = {
   Job: [],
   P_DELIVERY:'',
  isLoading: false,
  error: false,
};

const JobReducers = (state = defaultState, action) => {
  switch (action.type) {
    case 'FETCH_SUCCEEDED':
               let thamso = ""
               action.receivedJob.map(e=>{
                if(e.status==='P'){
                  thamso= e.delivery_id;
                }
            });                           
      return {
              Job: action.receivedJob,
              P_DELIVERY:thamso,
              // P_DELIVERY: action.receivedJob.map(e=>{
              //     if(e.status==='P') return e.delivery_id;
              // }),
              error: false, isLoading: false};
    case 'FETCH_FAILED':
      return {Job: [], error: true, isLoading: false};
      return state;
      
    case 'COMPLETEDJOB':
      // console.log('#JobReducer', action.oder_action)
      state.Job.map(e => {
        // console.log('#jobReducer loop e = ', e.Oder_id);
        e.oder_detail.map(e1 => {
          // console.log('#jobReducer loop e = ', e1.Oder_detail_id);
          if (e1.Oder_detail_id === action.oder_action) {
            e1.status = 'COMPLETED';
          }
        });
      });

      return state;

    default:
      return state; //state does not change
  }
};

export default JobReducers;
