// import { delay } from 'redux-saga';
// import { all } from 'redux-saga/effects';

// import { sayHello } from './counterSagas';
// import {  watchIncrement,watchDecrement } from './counterSagas';

// export default function* rootSaga() {
//     yield all([
//         // sayHello(),
//         watchIncrement(),
//         watchDecrement(),
//     ]);
// }

import { all, fork } from 'redux-saga/effects';
import { watchLogin } from './Jobsagas';
import { watchGETLVC } from './Jobsagas';
import { watchLOADING_URL } from './Jobsagas';



export default function* rootSaga() {
  yield fork(watchLogin),
  yield fork(watchGETLVC)
  yield fork(watchLOADING_URL)
}
