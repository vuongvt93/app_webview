
import { Alert } from 'react-native';
import { put, takeLatest, select } from 'redux-saga/effects';
import { Api } from './Api';
import AsyncStorage from '@react-native-community/async-storage';
import { END_POINT, callApi, BASE_URL, LOADING_URL_MOBILE} from './common';
import { string } from 'prop-types';

function* LOGIN(action) {
    const { NAVIGATION, SETSTATE } = action;
    const {P_USERNAME}=action;
    try {
        // SETSTATE({ isLoading: true });
        const url = BASE_URL + END_POINT.login;
        const params = {
            P_USERNAME: action.P_USERNAME,
            P_PASSWORD: action.P_PASSWORD,
        };
        const data = yield callApi(url, 'POST', params);
        yield put({ type: 'LOGIN_SUCCEEDED', resultLogin: data,P_USERNAME:action.P_USERNAME });
        if (data.state === 'OK') {
            // const receivedJob = yield Api.getJobTypeApi(P_USERNAME);
            // const ES_Doc = yield Api.Get_Doc(P_USERNAME);
            // yield put({ type: 'FETCH_SUCCEEDED', receivedJob: receivedJob,});
            // const ES_Doc = yield Api.Get_Doc(P_USERNAME);
            // yield put({ type: 'ES_SUCCEEDED', ES_Doc: ES_Doc,});
            // SETSTATE({ isLoading: false });
            // NAVIGATION.navigate('QR_Delivery', { P_USERNAME: action.P_USERNAME });
        }
        // SETSTATE({ isLoading: false });
        // NAVIGATION.navigate('JobType', { P_USERNAME: action.P_USERNAME });
        Alert.alert('Đăng Nhập Thành Công');
    } catch (error) {
        yield put({ type: 'LOGIN_FAILED', error });

        // SETSTATE({ isLoading: false });
        // Alert.alert('Login fail');
        Alert.alert(error.error);
        // Alert.alert('Đăng Nhập Thành Công');
    }
}

export function* watchLogin() {
    yield takeLatest('LOGIN', LOGIN);
}

function* LOADING_URL(action) {
    const {SETSTATE } = action;
    // const {P_USERNAME}=action;
    try {
        // SETSTATE({ isLoading: true });
        // const url = LOADING_URL_MOBILE;
        // const params = {
        //     P_USERNAME: action.P_USERNAME,
        //     P_PASSWORD: action.P_PASSWORD,
        // };
        const receiveurl = yield callApi(LOADING_URL_MOBILE,'GET');
        // const receiveurl = yield Api.geturlmobile();
        yield put({ type: 'LOADURL_SUCCESS', URL: receiveurl });
        let urlmobile = ""
        receiveurl.map(e=>{
            if(e.name==='URL'){
                urlmobile= e.value;
                SETSTATE({ URL: urlmobile });
            }
        });     
        // if (data.name === 'URL') {
        //     const urlmobile = data.value;
        //     SETSTATE({ URL: urlmobile });
        //     // NAVIGATION.navigate('QR_Delivery', { P_USERNAME: action.P_USERNAME });
        // }
        // SETSTATE({ isLoading: false });
        // NAVIGATION.navigate('JobType', { P_USERNAME: action.P_USERNAME });
        // Alert.alert('Đăng Nhập Thành Công');
    } catch (error) {
        yield put({ type: 'LOADURL_SUCCESS', error });

        // SETSTATE({ isLoading: false });
        // Alert.alert('Login fail');
        // Alert.alert(error.error);
        // Alert.alert('Đăng Nhập Thành Công');
    }
}

export function* watchLOADING_URL() {
    yield takeLatest('LOADING_URL', LOADING_URL);
}

function* GETLVC(action) {
    const { NAVIGATION, SETSTATE } = action;
    const {P_USERNAME}=action;
    try {
        // SETSTATE({ isLoading: true });
        // const url = BASE_URL + END_POINT.login;
        const params = {
            P_USERNAME: action.P_USERNAME,          
        };
        // const data = yield callApi(url, 'POST', params);
        const receivedJob = yield Api.getJobTypeApi(P_USERNAME);
        yield put({ type: 'FETCH_SUCCEEDED', receivedJob: receivedJob,});       
        // yield put({ type: 'LOGIN_SUCCEEDED', resultLogin: data,P_USERNAME:action.P_USERNAME });
        if (receivedJob != 0) {
            // const receivedJob = yield Api.getJobTypeApi(P_USERNAME);
            // const ES_Doc = yield Api.Get_Doc(P_USERNAME);
            // yield put({ type: 'FETCH_SUCCEEDED', receivedJob: receivedJob,});
            // const ES_Doc = yield Api.Get_Doc(P_USERNAME);
            // yield put({ type: 'ES_SUCCEEDED', ES_Doc: ES_Doc,});
            // SETSTATE({ isLoading: false });
            // NAVIGATION.navigate('JobType', { P_USERNAME: action.P_USERNAME });
        }
        else {
            // SETSTATE({ isLoading: false });
            // NAVIGATION.navigate('JobType', { P_USERNAME: action.P_USERNAME });
            Alert.alert('Chưa có lệnh vận chuyển gán cho user này');
        }
        
    } catch (error) {
        yield put({ type: 'LOGIN_FAILED', error });
        // SETSTATE({ isLoading: false });       
        Alert.alert(error.error);
       
    }
}

export function* watchGETLVC() {
    yield takeLatest('GETLVC', GETLVC);
}


