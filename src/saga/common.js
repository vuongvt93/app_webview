import axios from 'axios';
import {Alert} from 'react-native';
import RNExitApp from 'react-native-exit-app';

export const BASE_URL = `https://hcerp.vn/ords/retail/Delivery/`;
export const LOADING_URL_MOBILE = `https://hcerp.vn/ords/retail/Mobile/GetParam`;
export const END_POINT = {
  login: `Login?`,
};

export async function callApi(url, method = 'get', params = {}, body = {}, header = {},) {
  let token = '';
  try {
    token = await getToken();
  } catch (error) {
    return error;
    // break;
  }
  console.log('request: ', { token, url, method, body, params,header });
  return axios({
    url,
    method,
    headers: {
      Authorization: 'Bearer ' + token,
      ...header,
    },
    data: method === 'get' ? '' : body,
    timeout: 10000,
    params,
  })
    .then(res => {
      console.log('response ss:', res);
      // console.log('response ss:', res.data);
      const URL = res.status === 200 ? res.data.data :
        Alert.alert('Kiểm tra kết nối Server');
        return URL;
      // const data = res.data;
      // const { state } = res.data;
      // if (state === 'OK') {
      //   return data;
      // }
      const dataRes = { response: res };
      throw dataRes;
    })
    .catch(error => {
      // console.log(`request error`, error.response);
      if (error.response && error.response.data) {
        throw error.response.data;
      }
      throw error;
    });
}

function getToken() {
  const url = `https://hcerp.vn/ords/retail/oauth/token?grant_type=client_credentials`;
  //   console.log('function getToken: ', url);
  return axios({
    url,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Basic dElYNG52TEJ3UndhN0xnTkZMT01vUS4uOkl5NFpOcFcwZzNudGdyNFRGczZKTFEuLg==`,
    },
    data: {
      grant_type: 'client_credentials',
      scope: 'test',
    },
    timeout: 10000,
  })
    .then(res => {
        console.log('response ss:', res.data.access_token);
      const token = res.data.access_token;
      return token;
    })
    .catch(error => { 
      // console.log('code da chay toi day');
      Alert.alert(
        'Kiểm tra kết nối Server',
        'Thử đăng nhập lại',
        [
          {text: 'Yes', onPress: () => RNExitApp.exitApp()},
        ],
        { cancelable: false });
    });
}
