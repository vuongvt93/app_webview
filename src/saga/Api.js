import axios from 'axios';
import { Token } from './gettoken';
import { Alert } from 'react-native';
// import StartJob from '../redux/reducer/StartJob';

const urlLogin = 'https://hcerp.vn/ords/retail/Delivery/Login?';
const urlloadmobile = 'https://hcerp.vn/ords/retail/Mobile/GetParam';

import { END_POINT, callApi, BASE_URL } from './common';


function* Login(P_USERNAME, P_PASSWORD) {
  try {
    const url = BASE_URL + END_POINT.login;

    const params = { P_USERNAME: P_USERNAME, P_PASSWORD: P_PASSWORD };
    const data = yield callApi(url, 'POST', params);
    this.setState({ isLoading: false });
    // console.log(`login`, data);
    this.props.navigation.navigate('JobType', { P_USERNAME: P_USERNAME });
  } catch (error) {

  }
}

function* getJobTypeApi(P_USERNAME) {
  // const TOKEN = yield Token.getNewToken();
  const TOKEN = yield Token.getToken();
  const response = yield axios(urlGetData, {
    method: 'GET',
    params:{P_USERNAME:P_USERNAME},
    headers: {
      Authorization: 'Bearer ' + TOKEN,
    },
    data: '',
  });
  // console.log(response);
  const JobType = yield response.status === 200 ? response.data.data : [];
  // console.log('show ket qua',JobType.length);
  return JobType;
}

function* geturlmobile() {
  // const TOKEN = yield Token.getNewToken();
  const TOKEN = yield Token.getToken();
  const response = yield axios(urlloadmobile, {
    method: 'GET',
    // params:{P_USERNAME:P_USERNAME},
    headers: {
      Authorization: 'Bearer ' + TOKEN,
    },
    data: '',
  });
  // console.log(response);
  const URL = yield response.status === 200 ? response.data.data :
   Alert.alert('Kiểm tra kết nối Internet');
  // console.log('show ket qua',JobType.length);
  return URL;
}



export const Api = {
  getJobTypeApi,
  Login,
  geturlmobile,
};
