import axios from 'axios';
import {Alert} from 'react-native';
import RNExitApp from 'react-native-exit-app';
// function* getNewToken() {
//     const response = yield axios('https://hcerp.vn/ords/retail/oauth/token?grant_type=client_credentials', {
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/json',
//             Authorization: `Basic dElYNG52TEJ3UndhN0xnTkZMT01vUS4uOkl5NFpOcFcwZzNudGdyNFRGczZKTFEuLg==`,
//         },
//         data: {
//             grant_type: 'client_credentials',
//             scope: 'test',
//         },

//     });
//     console.log(response.data.access_token);
//     const token = response.data.access_token
//     return token;
// }

function getToken() {
    const url = `https://hcerp.vn/ords/retail/oauth/token?grant_type=client_credentials`;
    //   console.log('function getToken: ', url);
    return axios({
      url,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Basic dElYNG52TEJ3UndhN0xnTkZMT01vUS4uOkl5NFpOcFcwZzNudGdyNFRGczZKTFEuLg==`,
      },
      data: {
        grant_type: 'client_credentials',
        scope: 'test',
      },
      timeout: 10000,
    })
      .then(res => {
          console.log('response ss:', res.data.access_token);
        const token = res.data.access_token;
        return token;
      })
      .catch(error => {        
        Alert.alert(
          'Lỗi kết nối',
          'Thử đăng nhập lại',
          [
            {text: 'Yes', onPress: () => RNExitApp.exitApp()},
          ],
          { cancelable: false });
      });
  }
  

export const Token = {
    // getNewToken
    getToken
}; 