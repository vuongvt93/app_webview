import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import MainNavigator from './src/AppNavigation';
import {Provider} from 'react-redux';
import store from './src/redux/store';

const AppContainer = createAppContainer(MainNavigator);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}
